package com.revo

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseFactory {

    private const val CREATE_INITIAL_TABLES_SCRIPT = """
                CREATE TABLE TRANSFER (
                    id UUID,
                    amount DECIMAL(20, 2),
                    destination UUID,
                    origin UUID,
                    created_at timestamp);

                CREATE TABLE USER (
                    id UUID,
                    name text,
                    created_at timestamp);

                CREATE TABLE ACCOUNT (
                    id UUID,
                    user_id UUID,
                    balance DECIMAL(20, 2),
                    created_at timestamp);

                INSERT INTO USER VALUES('a2533965-b4b5-42ae-a315-10332b935107', 'Lucas Magalhaes', now());
                INSERT INTO USER VALUES('a2533965-b4b5-42ae-a315-10332b935108', 'Revolut Hire me', now());

                INSERT INTO ACCOUNT VALUES(
                        'ba9f0e17-b21e-459d-bbac-a9e6f83d28ce',
                        'a2533965-b4b5-42ae-a315-10332b935107',
                         100.0,
                         now());

                 INSERT INTO ACCOUNT VALUES(
                        'bab9fe54-11db-4f73-8ce5-0372013f6e9c',
                        'a2533965-b4b5-42ae-a315-10332b935108',
                         150.0,
                         now());
                    """

    fun init() {
        Database.connect(hikari())
        transaction {
            this.exec(CREATE_INITIAL_TABLES_SCRIPT.trimIndent())
        }
    }

    private fun hikari(): HikariDataSource {
        val config = HikariConfig()
        config.driverClassName = "org.h2.Driver"
        config.jdbcUrl = "jdbc:h2:mem:test"
        config.maximumPoolSize = 100
        config.isAutoCommit = false
        config.transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        config.validate()
        return HikariDataSource(config)
    }

}