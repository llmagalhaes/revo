package com.revo.model.domain

import java.math.BigDecimal
import java.time.Instant
import java.util.*

data class Balance(
    val id: UUID,
    val userId: UUID,
    val balance: BigDecimal,
    val createdAt: Instant
)