package com.revo

import io.ktor.application.Application
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {
    @Test
    fun testBalance() {
        withTestApplication(Application::module) {
            with(handleRequest(HttpMethod.Get, "/v1/account/a2533965-b4b5-42ae-a315-10332b935107")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }
}
