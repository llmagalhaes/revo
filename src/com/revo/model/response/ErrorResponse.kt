package com.revo.model.response

data class ErrorResponse(
    val code: ErrorCodes,
    val message: String
)