package com.revo.model.domain

import java.time.Instant
import java.util.*

data class User(
    val id: UUID,
    val name: String,
    val createdAt: Instant? = Instant.now()
)