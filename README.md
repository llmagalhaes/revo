
**

## Why?
  
* **Kotlin**: I think Kotlin is a great language to write applications very fast and is less verbose then Java and is very similar to Java and also run on JVM :)  
* **Ktor**: Ktor is a lightweight framework by Jetbrains and it's very simple to use and install. There are no concept of Controller on Ktor but I have decided to use a package called  
controller to be easier to maintain and read by Java developers and devs who came from others MVC frameworks  
* **Koin**: Koin is a dependency inject framework very simple and pretty easy to read  
* **Insert only database**: As you can see, for every transfer there's a new account, on my experience on financial systems I have realized how crucial is to have all the informations  
on your database, of course there's lot of good practices to follow but for the sake of simplicity I didn't put it here.  
* **Exposed**: A database framework by jetbrain, I didn't use their ORM because it's not good for performance so I have decided to use native SQL, I also have developed some Kotlin extensions to be easier to query  
* **Coroutine**: The app should handle many requests so I couldn't let application hold threads. The best option here would be use an external message application like SQS, **RabbitMQ** or similar.  
  
** There's a bug on H2DB and I couldn't make use of prepared statement that's the reason why params are concateneted on query.

## * How to run?
clone repo and run java -jar rev-app.jar on main folder
  
  

## * How to make a test transfer?


> curl -X POST \    
> http://0.0.0.0:8080/v1/account/a2533965-b4b5-42ae-a315-10332b935107 \ 
> -H 'cache-control: no-cache' \     -H 'content-type: application/json' \     -d '{      "transfer":{  
>       "destination" : "a2533965-b4b5-42ae-a315-10332b935108",  
>       "amount": 10.0      }   }'

  
* How to check balance?  

> curl -X GET \    
> http://0.0.0.0:8080/v1/account/a2533965-b4b5-42ae-a315-10332b935107

  
  

## API Doc:

**REQUEST: GET - v1/account/{ID}**  
ID: Account ID  
Example: http://0.0.0.0:8080/v1/account/a2533965-b4b5-42ae-a315-10332b935107  
  
RESPONSE:  
**200:**  

> {   "userName": "Lucas Magalhaes",   "balance": 100,
>    "lastUpdate": {
>       "nano": 678000000,  
>      "epochSecond": 1558386497  
>      }   
>     }

  
**204:**  
User not found  
Example: http://0.0.0.0:8080/v1/account/a2533965-b4b5-42ae-a315-10332b935107  
  
**Why 204 and not 404?**  
404 can be easily mistaken by resource not found and can let client developers to false errors, according to RFC2616,  
404 is used on permanently unavailable resource which is not true on our case. Of course this is contract and can be implemented according to company patterns.  
 
 **REQUEST: POST - v1/account/{ID}**
 Body:

> {
	"transfer":{
		"destination" : "a2533965-b4b5-42ae-a315-10332b935108",
		"amount": 10.0
	}
}

200:

> {
    "transferId": "0c2fe7ea-eec2-4287-bf6d-98accadb9bc1",
    "amount": 10,
    "createdAt": {
        "nano": 108585000,
        "epochSecond": 1558387852
    }
}

400 - Default Payload: Check error list below
{
    "code": "NO_ENOUGH_BALANCE",
    "message": "Origin user does not have enough balance"
}

## Error codes
* NO_ENOUGH_BALANCE** - Origin user does not have enough balance
* INVALID_USER** - Invalid origin or destination user
* INVALID_AMOUNT** - Invalid amount