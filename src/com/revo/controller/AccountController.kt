package com.revo.controller

import com.revo.exception.InvalidRequest
import com.revo.model.request.AccountRequest
import com.revo.model.response.ErrorCodes
import com.revo.model.response.ErrorResponse
import com.revo.model.response.TransferResponse
import com.revo.service.AccountService
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import org.koin.ktor.ext.inject
import java.util.*

fun Route.accountRoutes() {

    val accountService: AccountService by inject()

    route("/account") {

        get("/{id}") {
            val accountId = UUID.fromString(call.parameters["id"])
            accountService.retrieveAccountBalance(accountId)?.let { call.respond(it) }
                ?: call.respond(
                    HttpStatusCode.NoContent,
                    ErrorResponse(ErrorCodes.INVALID_USER, ErrorCodes.INVALID_USER.message)
                )
        }

        post<AccountRequest>("/{id}") { request ->
            val requester = UUID.fromString(call.parameters["id"])
            try {
                call.respond(
                    TransferResponse(
                        accountService.transfer(request, requester),
                        request.transfer.amount
                    )
                )
            } catch (e: InvalidRequest) {
                call.respond(
                    HttpStatusCode.BadRequest, ErrorResponse(
                        e.error,
                        e.error.message
                    )
                )
            }
        }
    }

}