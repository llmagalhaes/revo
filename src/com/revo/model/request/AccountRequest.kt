package com.revo.model.request

import java.math.BigDecimal
import java.util.*

data class AccountRequest(
    val transfer: Transfer
)

data class Transfer(
    val destination: UUID,
    val amount: BigDecimal
)