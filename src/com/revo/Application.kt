package com.revo

import com.fasterxml.jackson.databind.SerializationFeature
import com.revo.controller.v1.v1
import com.revo.repository.AccountRepository
import com.revo.repository.TransferRepository
import com.revo.repository.UserRepository
import com.revo.service.AccountService
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DataConversion
import io.ktor.jackson.jackson
import io.ktor.request.path
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.slf4j.event.Level

fun main() {
    embeddedServer(
        factory = Netty,
        watchPaths = listOf("com.revo"),
        port = 8080,
        module = Application::module
    ).apply { start(wait = false) }
}

fun Application.module() {

    val appModules = module {
        factory { AccountRepository() }
        factory { UserRepository() }
        factory { TransferRepository() }
        factory { AccountService(get(), get(), get()) }
    }

    DatabaseFactory.init()

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(Koin) { modules(appModules) }
    install(DataConversion)
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    routing {
        v1()
    }
}