package com.revo.exception

import com.revo.model.response.ErrorCodes
import java.lang.RuntimeException

class InvalidRequest(val error: ErrorCodes) : RuntimeException()
