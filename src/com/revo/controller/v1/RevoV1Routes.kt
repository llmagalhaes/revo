package com.revo.controller.v1

import com.revo.controller.accountRoutes
import io.ktor.routing.Route
import io.ktor.routing.route

fun Route.v1() {
    route("v1") {
        accountRoutes()
    }
}