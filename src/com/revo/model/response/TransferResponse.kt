package com.revo.model.response

import java.math.BigDecimal
import java.time.Instant
import java.util.*

data class TransferResponse(
    val transferId: UUID,
    val amount: BigDecimal,
    val createdAt: Instant = Instant.now()
)