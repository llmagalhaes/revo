package com.revo.repository

import com.revo.extension.execAndMap
import com.revo.model.domain.User
import java.util.*

class UserRepository {
    fun exists(userId: UUID): Boolean = "SELECT COUNT(1) as exist FROM USER WHERE ID = '$userId'"
        .execAndMap { it.getBoolean("exist") }[0]

    fun find(userId: UUID): User {
        return "SELECT * FROM USER WHERE ID ='$userId'".execAndMap { rs ->
            User(
                rs.getObject("id", UUID::class.java),
                rs.getString("name"),
                rs.getTimestamp("created_at").toInstant()
            )
        }[0]
    }
}
