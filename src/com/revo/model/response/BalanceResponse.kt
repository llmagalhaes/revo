package com.revo.model.response

import java.math.BigDecimal
import java.time.Instant
import java.util.*

data class BalanceResponse(
    val userName: String,
    val balance: BigDecimal,
    val lastUpdate: Instant
)