package com.revo.model.response

enum class ErrorCodes(val message: String) {
    NO_ENOUGH_BALANCE("Origin user does not have enough balance"),
    INVALID_USER("Invalid origin or destination user"),
    INVALID_AMOUNT("Invalid amount")
}
