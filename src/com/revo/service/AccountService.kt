package com.revo.service

import com.revo.exception.InvalidRequest
import com.revo.model.request.AccountRequest
import com.revo.model.request.Transfer
import com.revo.model.response.BalanceResponse
import com.revo.model.response.ErrorCodes
import com.revo.repository.AccountRepository
import com.revo.repository.TransferRepository
import com.revo.repository.UserRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*

class AccountService(
    private val accountRepository: AccountRepository,
    private val userRepository: UserRepository,
    private val transferRepository: TransferRepository
) {

    fun retrieveAccountBalance(userId: UUID): BalanceResponse? {
        return accountRepository.findByUserId(userId)?.let {
            val userName = userRepository.find(userId).name
            BalanceResponse(
                userName,
                it.balance,
                it.createdAt
            )
        }
    }

    fun transfer(transferRequest: AccountRequest, requesterId: UUID): UUID {
        validate(transferRequest.transfer, requesterId)
        return performAsyncTransfer(transferRequest.transfer, requesterId) {
            with(transferRequest.transfer) {
                accountRepository.updateBalance(requesterId, this.amount.negate())?.let {
                    accountRepository.updateBalance(this.destination, this.amount)
                }
            }

        }
    }

    private fun performAsyncTransfer(transfer: Transfer, requesterId: UUID, function: () -> Unit): UUID {
        GlobalScope.launch {
            function.invoke()
        }
        return transferRepository.create(transfer, requesterId)
    }

    private fun validate(transfer: Transfer, requesterId: UUID) {
        if (transfer.amount <= BigDecimal.ZERO) throw InvalidRequest(ErrorCodes.INVALID_AMOUNT)
        if (transfer.destination == requesterId) throw InvalidRequest(ErrorCodes.INVALID_USER)
        if (!enoughBalance(requesterId, transfer.amount)) throw InvalidRequest(ErrorCodes.NO_ENOUGH_BALANCE)
        if (!validUser(transfer.destination)) throw InvalidRequest(ErrorCodes.INVALID_USER)
    }

    private fun enoughBalance(requesterId: UUID, amount: BigDecimal) = accountRepository.balance(requesterId) >= amount
    private fun validUser(destination: UUID) = userRepository.exists(destination)

}