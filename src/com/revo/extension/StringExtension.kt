package com.revo.extension

import org.jetbrains.exposed.sql.transactions.TransactionManager
import java.sql.ResultSet

fun <T:Any> String.execAndMap(params: Array<String>? = arrayOf(), transform : (ResultSet) -> T) : List<T> {
    return TransactionManager.manager.newTransaction().query(this, params, transform)
}