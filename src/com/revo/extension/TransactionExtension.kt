package com.revo.extension

import org.jetbrains.exposed.sql.Transaction
import java.sql.ResultSet

fun <T : Any> Transaction.query(sql: String, params: Array<String>? = arrayOf(), transform: (ResultSet) -> T): ArrayList<T> {
    val result = arrayListOf<T>()
    if (sql.contains("SELECT")) {
        connection.prepareStatement(sql, params).run {
            with(this.executeQuery()) {
                while (this.next()) {
                    result += transform(this)
                }
            }
        }
    } else {
        this.exec(sql)
        connection.commit()
    }
    return result
}