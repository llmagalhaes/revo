package com.revo.repository

import com.revo.extension.execAndMap
import com.revo.model.request.Transfer
import java.util.*

class TransferRepository {
    fun create(transferRequest: Transfer, origin: UUID): UUID {
        val transferId = UUID.randomUUID()
        """INSERT INTO TRANSFER VALUES(
            '$transferId',
            ${transferRequest.amount},
            '${transferRequest.destination}',
            '$origin',
            now())
        """.execAndMap {  }
        return transferId
    }
}