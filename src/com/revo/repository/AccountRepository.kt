package com.revo.repository

import com.revo.extension.execAndMap
import com.revo.model.domain.Balance
import java.math.BigDecimal
import java.util.*

class AccountRepository {

    fun balance(requesterId: UUID): BigDecimal =
        "SELECT BALANCE FROM ACCOUNT WHERE USER_ID = '$requesterId' ORDER BY CREATED_AT DESC LIMIT 1"
            .execAndMap { it.getBigDecimal("balance") }[0]

    fun findByUserId(userId: UUID): Balance? {
        return try {
            "SELECT * FROM ACCOUNT WHERE USER_ID = '$userId' ORDER BY CREATED_AT DESC LIMIT 1".execAndMap { rs ->
                Balance(
                    rs.getObject("id", UUID::class.java),
                    rs.getObject("user_id", UUID::class.java),
                    rs.getBigDecimal("balance"),
                    rs.getTimestamp("created_at").toInstant()
                )
            }[0]
        } catch (e: Exception) {
            return null
        }
    }

    fun updateBalance(destination: UUID, amount: BigDecimal): UUID? {
        val currentAmount = balance(destination)
        val newAccountId = UUID.randomUUID()
        return try {
            "INSERT INTO ACCOUNT VALUES ('$newAccountId', '$destination', ${amount.plus(currentAmount)}, now()) ".execAndMap {}
            newAccountId
        } catch (e: Exception) {
            throw RuntimeException("Fail on transfer")
        }
    }

}